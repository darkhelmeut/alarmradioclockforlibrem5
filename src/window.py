# window.py
#
# Copyright 2023 theDarky
#
# This program is free software: you can redistribute it and/or modify
# it under the terms of the GNU General Public License as published by
# the Free Software Foundation, either version 3 of the License, or
# (at your option) any later version.
#
# This program is distributed in the hope that it will be useful,
# but WITHOUT ANY WARRANTY; without even the implied warranty of
# MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
# GNU General Public License for more details.
#
# You should have received a copy of the GNU General Public License
# along with this program.  If not, see <http://www.gnu.org/licenses/>.

from gi.repository import Gtk


@Gtk.Template(resource_path='/travelpandas/radioalarm/theDarky/radio.ui')
class RadioalarmWindow(Gtk.ApplicationWindow):
    __gtype_name__ = 'RadioalarmWindow'

    data = {
        'nostalgie': 'http://cdn.nrjaudio.fm/audio1/fr/30601/mp3_128.mp3', 
        'france info': 'http://direct.franceinfo.fr/live/franceinfo-midfi.mp3', 
        'rire & chansons': 'http://cdn.nrjaudio.fm/audio1/fr/30401/mp3_128.mp3'
    }
    
    theList = Gtk.Template.Child()
    selectDropDown = Gtk.Template.Child()

    def __init__(self, **kwargs):
        super().__init__(**kwargs)
        for key, value in self.data.items():
            self.theList.append(f"{key}")

    @Gtk.Template.Callback()
    def show_leading_zeros(self, spin_button):
        adjustment = spin_button.get_adjustment()
        spin_button.set_text('{:02d}'.format(int(adjustment.get_value())))
        return True

    @Gtk.Template.Callback()
    def save_config(self, saveButton):
        selectedRadio = self.selectDropDown.get_selected_item().get_string()
        print(selectedRadio)
        print(self.data[selectedRadio])

class AboutDialog(Gtk.AboutDialog):

    def __init__(self, parent):
        Gtk.AboutDialog.__init__(self)
        self.props.program_name = 'radioalarm'
        self.props.version = "0.1.0"
        self.props.authors = ['theDarky']
        self.props.copyright = '2022 theDarky'
        self.props.logo_icon_name = 'travelpandas.radioalarm.theDarky'
        self.props.modal = True
        self.set_transient_for(parent)
